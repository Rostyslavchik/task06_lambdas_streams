package com.rostyslavprotsiv;

import com.rostyslavprotsiv.controller.Controller;
import com.rostyslavprotsiv.model.entity.MyFunc;
import com.rostyslavprotsiv.view.ClientMenu;
import com.rostyslavprotsiv.view.TaskFourMenu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
    private static Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        MyFunc max = (a, b, c) -> Math.max(Math.max(a, b), Math.max(b, c));
        MyFunc average = (a, b, c) -> (a + b + c) / 3;
        int maxV, avV;
        maxV = max.apply(8 ,6 ,14);
        avV = average.apply(3, 4, 52);
        logger.info("Max = " + maxV);
        logger.info("Average = " + avV);
//        ClientMenu menu = new ClientMenu();
//        menu.show();
        Controller controller = new Controller();
        controller.executeTextTask();
//        TaskFourMenu menu = new TaskFourMenu();
//        System.out.println(menu.inputText());
    }
}
