package com.rostyslavprotsiv.controller;

import com.rostyslavprotsiv.model.action.RandomIntegerGenerator;
import com.rostyslavprotsiv.model.action.StreamAction;
import com.rostyslavprotsiv.model.action.TextStreamAction;
import com.rostyslavprotsiv.view.Menu;
import com.rostyslavprotsiv.view.TaskFourMenu;

import java.util.Arrays;
import java.util.List;

public class Controller {
    private static final StreamAction STREAM_ACTION = new StreamAction();
    private static final TextStreamAction TEXT_STREAM_ACTION
            = new TextStreamAction();
    private static final Menu MENU = new Menu();
    private static final TaskFourMenu TASK_FOUR_MENU = new TaskFourMenu();
    private static final RandomIntegerGenerator GENERATOR
            = new RandomIntegerGenerator();


    public void execute() {
        List<Integer> list = GENERATOR.generateList();
        Integer[] arr = GENERATOR.generateArray();
        double averageOnList = STREAM_ACTION.countAverageOnList(list);
        double averageOnArray = STREAM_ACTION.countAverageOnArray(arr);
        int minOnList = STREAM_ACTION.countMinOnList(list);
        int minOnArray = STREAM_ACTION.countMinOnArray(arr);
        int maxOnList = STREAM_ACTION.countMaxOnList(list);
        int maxOnArray = STREAM_ACTION.countMaxOnArray(arr);
        int sumOnList = STREAM_ACTION.countSumOnList(list);
        int sumOnArray = STREAM_ACTION.countSumOnArray(arr);
        int sumReduceOnList = STREAM_ACTION.countSumReduceOnList(list);
        int sumReduceOnArray = STREAM_ACTION.countSumReduceOnArray(arr);
        long biggerThanAverageList
                = STREAM_ACTION.countBiggerThanAverageList(list);
        long biggerThanAverageArray
                = STREAM_ACTION.countBiggerThanAverageArray(arr);
        MENU.welcome();
        MENU.outGeneratedList(list.toString());
        MENU.outGeneratedArray(Arrays.toString(arr));
        MENU.outAverageForList(String.valueOf(averageOnList));
        MENU.outAverageForArray(String.valueOf(averageOnArray));
        MENU.outMinForList(String.valueOf(minOnList));
        MENU.outMinForArray(String.valueOf(minOnArray));
        MENU.outMaxForList(String.valueOf(maxOnList));
        MENU.outMaxForArray(String.valueOf(maxOnArray));
        MENU.outSumForList(String.valueOf(sumOnList));
        MENU.outSumForArray(String.valueOf(sumOnArray));
        MENU.outSumWithReduceForList(String.valueOf(sumReduceOnList));
        MENU.outSumWithReduceForArray(String.valueOf(sumReduceOnArray));
        MENU.outBiggerThanAverageList(String.valueOf(biggerThanAverageList));
        MENU.outBiggerThanAverageArray(String.valueOf(biggerThanAverageArray));
    }

    public void executeTextTask() {
        String text;
        TASK_FOUR_MENU.welcome();
        text = TASK_FOUR_MENU.inputText();
        TASK_FOUR_MENU.outUniqueWordsNumber(String.valueOf(
                TEXT_STREAM_ACTION.getUniqueWordsNumber(text)));
        TASK_FOUR_MENU.outUniqueWordsSortedList(
                TEXT_STREAM_ACTION.getUniqueWordsSortedList(text).toString());
        TASK_FOUR_MENU.outWordsOccurrences(
                TEXT_STREAM_ACTION
                        .getEachWordOccurrenceNumber(text).toString());
        TASK_FOUR_MENU.outSymbolOccurrencesExceptUpperCase(
                TEXT_STREAM_ACTION
                        .getEachSymbolOccurrenceNumber(text).toString());
    }
}
