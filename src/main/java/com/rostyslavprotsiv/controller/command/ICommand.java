package com.rostyslavprotsiv.controller.command;

@FunctionalInterface
public interface ICommand<T> {
    void execute(T param);
}
