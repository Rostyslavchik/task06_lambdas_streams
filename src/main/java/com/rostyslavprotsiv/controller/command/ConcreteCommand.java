package com.rostyslavprotsiv.controller.command;

import com.rostyslavprotsiv.model.entity.Receiver;

public class ConcreteCommand<T> implements ICommand<T> {
    private final Receiver receiver;

    public ConcreteCommand(Receiver receiver) {
        this.receiver = receiver;
    }

    @Override
    public void execute(T param) {
        receiver.operation(param);
    }
}
