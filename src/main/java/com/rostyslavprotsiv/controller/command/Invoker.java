package com.rostyslavprotsiv.controller.command;

import com.rostyslavprotsiv.controller.command.ICommand;

public class Invoker<T> {
    private ICommand<T> command;

    public Invoker() {}

    public Invoker(ICommand<T> command) {
        this.command = command;
    }

    public void setCommand(ICommand<T> command) {
        this.command = command;
    }

    public void executeCommand(T param) {
        command.execute(param);
    }
}
