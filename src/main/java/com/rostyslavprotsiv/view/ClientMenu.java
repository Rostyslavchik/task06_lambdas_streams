package com.rostyslavprotsiv.view;

import com.rostyslavprotsiv.controller.command.ConcreteCommand;
import com.rostyslavprotsiv.controller.command.ICommand;
import com.rostyslavprotsiv.controller.command.Invoker;
import com.rostyslavprotsiv.model.entity.Receiver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class ClientMenu {
    private Map<String, String> menu;
    private Map<String, Invoker<String>> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger(ClientMenu.class);

    public ClientMenu() {
        initMenu();
        initMethodsMenu();
    }

    public void welcome() {
        logger.info("Welcome to the Command testing task.");
        logger.info("Please, input the next to test the software: ");
    }

    private void initMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", "Test Command as lambda function");
        menu.put("2", "Test Command as method reference");
        menu.put("3", "Test Command as anonymous class");
        menu.put("4", "Test Command as object of command class");
        menu.put("Q", "Q - exit");
    }

    private void initMethodsMenu() {
        Receiver receiver = new Receiver();
        ICommand<String> lambdasCommand = (str)
                -> receiver.<String>operation(str);
        Invoker<String> lambdas = new Invoker<String>(lambdasCommand);
        Invoker<String> methodReference =
                new Invoker<String>(receiver::operation);
        Invoker<String> anonymousClass =
                new Invoker<String>(new ICommand<String>() {
            @Override
            public void execute(String param) {
                receiver.<String>operation(param);
            }
        });
        Invoker<String> concreteObject = new Invoker<String>(
                new ConcreteCommand<String>(receiver));

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", lambdas);
        methodsMenu.put("2", methodReference);
        methodsMenu.put("3", anonymousClass);
        methodsMenu.put("4", concreteObject);
    }

    public void show() {
        String menuKey;
        String message;
        welcome();
        do {
            outputMenu();
            logger.info("Please, select menu point.");
            menuKey = input.nextLine().toUpperCase();
            logger.info("Please, input some line");
            message = input.nextLine();
            try {
                methodsMenu.get(menuKey).executeCommand(message);
            } catch (Exception e) {
                logger.error("Something happened " + e.getMessage()
                        + " " + e);
            }
        } while (!menuKey.equals("Q"));
    }

    private void outputMenu() {
        System.out.println("\nMenu:");
        for(String str: menu.values()) {
            logger.info(str);
        }
    }
}
