package com.rostyslavprotsiv.view;

import com.rostyslavprotsiv.controller.command.ConcreteCommand;
import com.rostyslavprotsiv.controller.command.ICommand;
import com.rostyslavprotsiv.controller.command.Invoker;
import com.rostyslavprotsiv.model.entity.Receiver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Scanner;

public class Menu {
    private static Logger logger = LogManager.getLogger(Menu.class);

    public void welcome() {
        logger.info("Welcome to the Stream testing task.");
    }

    public void outGeneratedList(String list) {
        logger.info("Randomly generated list: ");
        logger.info(list);
    }

    public void outGeneratedArray(String arr) {
        logger.info("Randomly generated array: ");
        logger.info(arr);
    }

    public void outAverageForList(String average) {
        logger.info("Average for list: ");
        logger.info(average);
    }

    public void outAverageForArray(String average) {
        logger.info("Average for array: ");
        logger.info(average);
    }

    public void outMinForList(String min) {
        logger.info("Min for list: ");
        logger.info(min);
    }

    public void outMinForArray(String min) {
        logger.info("Min for array: ");
        logger.info(min);
    }

    public void outMaxForList(String max) {
        logger.info("Max for list: ");
        logger.info(max);
    }

    public void outMaxForArray(String max) {
        logger.info("Max for array: ");
        logger.info(max);
    }

    public void outSumForList(String sum) {
        logger.info("Sum for list: ");
        logger.info(sum);
    }

    public void outSumForArray(String sum) {
        logger.info("Sum for array: ");
        logger.info(sum);
    }

    public void outSumWithReduceForList(String sum) {
        logger.info("Sum with reduce for list: ");
        logger.info(sum);
    }

    public void outSumWithReduceForArray(String sum) {
        logger.info("Sum with reduce for array: ");
        logger.info(sum);
    }

    public void outBiggerThanAverageList(String total) {
        logger.info("Total amount of numbers that are bigger than"
                 + " average for list: ");
        logger.info(total);
    }

    public void outBiggerThanAverageArray(String total) {
        logger.info("Total amount of numbers that are bigger than"
                + " average for array: ");
        logger.info(total);
    }
}
