package com.rostyslavprotsiv.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class TaskFourMenu {
    private static Logger logger = LogManager.getLogger(TaskFourMenu.class);
    private static Scanner scan = new Scanner(System.in);

    public void welcome() {
        logger.info("Welcome to the Stream second testing task.");
    }

    public String inputText() {
        logger.info("Please, input some lines:");
        StringBuilder text = new StringBuilder();
        String line = "";
        do {
            text.append(line + "\n");
            line = scan.nextLine();
        } while (!line.isEmpty());
        logger.info("Thanks for the input!");
        return text.toString();
    }

    public void outUniqueWordsNumber(String number) {
        logger.info("Number of unique words: " + number);
    }

    public void outUniqueWordsSortedList(String list) {
        logger.info("Sorted list of unique words: " + list);
    }

    public void outWordsOccurrences(String info) {
        logger.info("Words occurrences: " + info);
    }

    public void outSymbolOccurrencesExceptUpperCase(String info) {
        logger.info("Symbol occurrences except "
                + "upper case characters: " + info);
    }
}
