package com.rostyslavprotsiv.model.action;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamAction {

    public double countAverageOnList(List<Integer> list) {
        return list.stream()
                .mapToInt(Integer::intValue)
                .average()
                .getAsDouble();
    }

    public double countAverageOnArray(Integer[] arr) {
        return Arrays.stream(arr)
                .mapToInt(Integer::intValue)
                .average()
                .getAsDouble();
    }

    public int countMinOnList(List<Integer> list) {
        return list.stream()
                .mapToInt(Integer::intValue)
                .min().getAsInt();
    }

    public int countMinOnArray(Integer[] arr) {
        return Arrays.stream(arr)
                .mapToInt(Integer::intValue)
                .min()
                .getAsInt();
    }

    public int countMaxOnList(List<Integer> list) {
        return list.stream()
                .mapToInt(Integer::intValue)
                .max().getAsInt();
    }

    public int countMaxOnArray(Integer[] arr) {
        return Arrays.stream(arr)
                .mapToInt(Integer::intValue)
                .max()
                .getAsInt();
    }

    public int countSumOnList(List<Integer> list) {
        return list.stream()
                .mapToInt(Integer::intValue)
                .sum();
    }

    public int countSumOnArray(Integer[] arr) {
        return Arrays.stream(arr)
                .mapToInt(Integer::intValue)
                .sum();
    }

    public int countSumReduceOnList(List<Integer> list) {
        return list.stream()
                .mapToInt(Integer::intValue)
                .reduce((a, b) -> a + b).getAsInt();
    }

    public int countSumReduceOnArray(Integer[] arr) {
        return Arrays.stream(arr)
                .mapToInt(Integer::intValue)
                .reduce((a, b) -> a + b).getAsInt();
    }

    public long countBiggerThanAverageList(List<Integer> list) {
        return list.stream()
                .filter(p -> p > countAverageOnList(list))
                .count();
    }

    public long countBiggerThanAverageArray(Integer[] arr) {
        return Arrays.stream(arr)
                .filter(p -> p > countAverageOnArray(arr))
                .count();
    }
}
