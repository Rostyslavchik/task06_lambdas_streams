package com.rostyslavprotsiv.model.action;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RandomIntegerGenerator {
    private static final Integer MIN_VALUE = -2;
    private static final Integer MAX_VALUE = 10;
    private static final Integer MAX_AMOUNT = 10;
    private static final Integer MIN_AMOUNT = 1;
    private static final Random RAND = new Random();

    public List<Integer> generateList() {
        return Stream.generate(() -> RAND
                        .nextInt(MAX_VALUE - MIN_VALUE) + MIN_VALUE)
                        .limit(RAND.nextInt(MAX_AMOUNT
                                - MIN_AMOUNT) + MIN_AMOUNT)
                        .collect(Collectors.toList());
    }

    public Integer[] generateArray() {
        int startPoint = RAND.nextInt(MAX_VALUE - MIN_VALUE) + MIN_VALUE;
        int step = RAND.nextInt(MAX_VALUE - MIN_VALUE) + MIN_VALUE;
        int amount = RAND.nextInt(MAX_AMOUNT - MIN_AMOUNT) + MIN_AMOUNT;
        return Stream.iterate(startPoint, n -> n + step)
                .limit(amount)
                .toArray(Integer[]::new);
    }
}
