package com.rostyslavprotsiv.model.action;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TextStreamAction {
    public long getUniqueWordsNumber(String text) {
        return Stream.of(text.split("\\W"))
                .filter(s -> !s.isEmpty())
                .distinct()
                .count();
    }

    public List<String> getUniqueWordsSortedList(String text) {
        return Stream.of(text.split("\\W"))
                .filter(s -> !s.isEmpty())
                .distinct()
                .sorted()
                .collect(Collectors.toList());
    }

    public Map<String, Long> getEachWordOccurrenceNumber(String text) {
        return Stream.of(text.split("\\W"))
                .filter(s -> !s.isEmpty())
                .collect(Collectors.groupingBy(
                        Function.identity(), Collectors.counting()));
    }

    public Map<Character, Long> getEachSymbolOccurrenceNumber(String text) {
        return text.replaceAll("\\s+", "").chars()
                .mapToObj(sym -> (char)sym)
                .filter(s -> s.toString().equals(s.toString().toLowerCase()))
                .collect(Collectors.groupingBy(
                s->s, Collectors.counting()));
    }
}
