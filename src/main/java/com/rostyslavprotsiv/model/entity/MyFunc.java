package com.rostyslavprotsiv.model.entity;

@FunctionalInterface
public interface MyFunc {
    int apply(int a, int b, int c);
}
