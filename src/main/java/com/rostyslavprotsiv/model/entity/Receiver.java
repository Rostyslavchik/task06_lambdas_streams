package com.rostyslavprotsiv.model.entity;

public class Receiver {
    public <T> void operation(T param) {
        System.out.println("Do Something... " + param);
    }
}
